from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.http import HttpRequest

# Create your tests here.
class indexTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super(indexTest, cls).setUpClass()
        cls.user = User.objects.create_user(
            'raihan', '1@gmail.com', '123'
        )
        cls.user.first_name = 'firstname'
        cls.user.last_name = 'lastname'
        cls.user.save()
        
    def test_homepage_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_loginpage_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_already_authenticated(self):
        request = HttpRequest()
        request.user = User()
        views.login(request)

    def test_login_post_succeed(self):
        response = self.client.post('/login/', {
                'username' : 'raihan',
                'password' : '123',
            })
        self.assertEqual(response.status_code, 302)
        response_home = self.client.get('/')
        html = response_home.content.decode()
        self.assertIn(self.user.first_name, html)

    def test_login_post_fail(self):
        response = self.client.post('/login/', {
             'username' : 'anonymousman22',
             'password' : 'passwordbigbrain1234',
            })
        self.assertEqual(response.status_code, 200)
        html = response.content.decode()
        self.assertIn('Login Page', html)
